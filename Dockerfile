FROM php:7.2-fpm-alpine3.8

RUN echo "http://mirrors.aliyun.com/alpine/v3.8/main/" > /etc/apk/repositories && \
	docker-php-ext-install mysqli pdo pdo_mysql && \
	echo "output_buffering=on" > /usr/local/etc/php/php.ini